const express = require('express');
const mongoose = require('mongoose');

const router = express.Router();
const checkAuth = require('../auth/check-auth');

const { Project } = require('../models/project');
const { Task } = require('../models/project');
const Log = require('../models/log');

router.get('/', async (req, res, next) => {
  try {
    const projects = await Project.find();
    res.status(200).json(projects);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.post('/', async (req, res, next) => {
  const project = new Project({
    _id: new mongoose.Types.ObjectId(),
    projectName: req.body.projectName,
    sections: req.body.sections,
  });

  try {
    const proj = await project.save();
    res.status(201).json(proj);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.get('/:projectId', async (req, res, next) => {
  const id = req.params.projectId;
  const projectTasks = await Task.find({ project: id });

  try {
    const project = await Project.findByIdAndUpdate(id, { tasks: projectTasks })
      .select('projectName sections tasks')
      .populate('tasks');

    if (project) {
      res.status(200).json(project);
    } else {
      res.status(404).json({ message: 'no project with that id' });
    }
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.put('/:projectId', async (req, res, next) => {
  const id = req.params.projectId;
  const newProject = req.body;

  try {
    const project = await Project.findById({ _id: id });

    const updatedProject = await Project.findByIdAndUpdate(id, newProject, {
      new: true,
    })
      .select('projectName sections tasks')
      .populate('tasks');

    const sectionsBefore = project.sections.map((section) => section.sectionName);
    const sectionsAfter = updatedProject.sections.map((section) => section.sectionName);
    const section = sectionsBefore.find((section) => !sectionsAfter.includes(section));
    if (section) {
      const sectionTasks = await Task.find({ section });

      await Task.deleteMany({ section });

      sectionTasks.map(async ({ _id }) => await Log.deleteMany({ taskId: _id }));
    }

    res.status(200).json(updatedProject);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.delete('/:projectId', async (req, res, next) => {
  const id = req.params.projectId;

  try {
    const deletedTasks = await Task.find({ project: id });

    await Task.deleteMany({ project: id });

    deletedTasks.map(async ({ _id }) => await Log.deleteMany({ taskId: _id }));

    await Project.deleteOne({ _id: id });

    res.status(200).json({ message: `Project with ID:${id} was successfully deleted` });
  } catch (error) {
    res.status(500).json({ message: 'Try later' });
  }
});

module.exports = router;
