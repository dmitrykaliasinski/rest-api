const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const Log = require('../models/log');

router.post('/', async (req, res, next) => {
  const { date, time, description, taskId } = req.body;

  const log = new Log({
    _id: new mongoose.Types.ObjectId(),
    date,
    time,
    description,
    taskId,
  });

  try {
    const newLog = await log.save();
    res.status(201).json(newLog);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.get('/', async (req, res, next) => {
  let { taskId, page, limit } = req.query;
  limit = limit || 5;
  page = page || 1;
  const offset = page * limit - limit;

  try {
    const logs = await Log.find({ taskId }).sort({ date: 'desc' }).limit(limit).skip(offset);
    const logCount = await Log.count({ taskId });
    res.status(200).json({ logs, logCount });
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.get('/time', async (req, res, next) => {
  let { taskId } = req.query;

  try {
    const logs = await Log.find({ taskId }).sort({ date: 'asc' });
    res.status(200).json(logs);
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;
