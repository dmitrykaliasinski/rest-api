const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

router.post('/signup', async (req, res, next) => {
  const isUser = await User.findOne({ email: req.body.email });

  if (isUser) {
    return res.status(409).json({ message: 'email exist' });
  } else {
    bcrypt.hash(req.body.password, 10, async (err, hash) => {
      if (err) {
        return res.status(500).json({ error: err });
      } else {
        const user = new User({
          _id: new mongoose.Types.ObjectId(),
          email: req.body.email,
          password: hash,
        });

        user
          .save()
          .then((result) => {
            res.status(201).json({
              message: 'New user was created',
            });
          })
          .catch((err) => {
            res.status(500).json({ error: err });
          });
      }
    });
  }
});

router.post('/login', async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: 'Auth failed' });
    }
    bcrypt.compare(password, user.password, (err, result) => {
      if (err || !result) {
        return res.status(401).json({ message: 'Auth failed' });
      }

      const token = jwt.sign(
        {
          email: user.email,
          id: user._id,
        },
        'securityWord',
        {
          expiresIn: '4h',
        }
      );

      res.status(200).json({ message: 'Authenticated', token });
    });
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.delete('/:userId', async (req, res, next) => {
  const id = req.params.userId;

  try {
    await User.remove({ _id: id });
    res.status(200).json({ message: 'user was deleted' });
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;
