const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const { Task } = require('../models/project');
const Log = require('../models/log');

router.get('/', async (req, res, next) => {
  try {
    const tasks = await Task.find();
    res.status(200).json(tasks);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.post('/', async (req, res, next) => {
  const task = new Task({
    project: req.body.project,
    title: req.body.title,
    description: req.body.description,
    section: req.body.section,
    index: req.body.index,
  });

  try {
    const newTask = await task.save();
    res.status(200).json(newTask);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.get('/:taskId', async (req, res, next) => {
  const taskId = req.params.taskId;

  try {
    const task = await Task.findById(taskId);

    if (task) {
      res.status(200).json(task);
    } else {
      res.status(404).json({ message: 'no task with that id' });
    }
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.put('/:taskId', async (req, res, next) => {
  const taskId = req.params.taskId;
  const updatedTask = req.body;

  try {
    const task = await Task.findByIdAndUpdate(taskId, updatedTask, { new: true });
    res.status(200).json(task);
  } catch (error) {
    res.status(500).json({ error });
  }
});

router.delete('/:taskId', async (req, res, next) => {
  const taskId = req.params.taskId;

  try {
    await Task.remove({ _id: taskId });
    await Log.remove({ taskId });
    res.status(200).json({ message: `task with id: ${taskId} was deleted`, _id: taskId });
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;
