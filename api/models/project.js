const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project',
  },
  title: { type: String, required: true },
  description: { type: String, required: true },
  section: { type: String, required: true },
  index: Number,
  estimate: { type: Number, default: 0 },
  logs: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Log',
    },
  ],
});

const sectionSchema = mongoose.Schema({
  sectionName: String,
});

const projectSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  projectName: { type: String, required: true },
  sections: [sectionSchema],
  tasks: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Task',
    },
  ],
});

const Task = mongoose.model('Task', taskSchema);
const Project = mongoose.model('Project', projectSchema);

module.exports = { Task, Project };
