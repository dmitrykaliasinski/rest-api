const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  time: { type: String, required: true },
  description: { type: String, required: true },
  date: { type: String, required: true },
  taskId: { type: mongoose.Schema.Types.ObjectId, ref: 'Task', required: true },
});

module.exports = mongoose.model('Log', userSchema);
