const express = require('express');
const morgan = require('morgan'); // Tool to see requests in terminal
const bodyParser = require('body-parser'); // to read body from req
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

const projectRoutes = require('./api/routes/projects');
const taskRoutes = require('./api/routes/task');
const userRoutes = require('./api/routes/user');
const logRoutes = require('./api/routes/log');

mongoose.connect('mongodb://localhost:27017');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Handling CORS
app.use((req, res, next) => {
  res.header('Access-Control-Alow-Origin', '*');
  res.header(
    'Access-Control-Alow-Headers',
    'Origin, X-Requested-Width, Content-Type, Accept, Authorization'
  );

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Alow-Methods', 'POST, PUT, GET, DELETE');
    res.status(200).json({});
  }

  next();
});

//Route that handle request
app.use('/projects', projectRoutes);
app.use('/user', userRoutes);
app.use('/tasks', taskRoutes);
app.use('/logs', logRoutes);

//Handling errors
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
